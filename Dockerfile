FROM php:8.1-fpm

RUN pear config-set php_ini "$PHP_INI_DIR"

# Copy composer.lock and composer.json into the working directory
COPY ./core/app/composer*.json  /var/www/

# Set working directory
WORKDIR /var/www/

RUN apt update && apt install -y unixodbc-dev gpg libzip-dev \
&& curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
&& curl https://packages.microsoft.com/config/debian/11/prod.list > /etc/apt/sources.list.d/mssql-release.list \
&& apt update \
&& ACCEPT_EULA=Y apt-get install -y msodbcsql18 \
&& pecl install sqlsrv \
&& pecl install pdo_sqlsrv \
&& docker-php-ext-install pdo opcache bcmath zip \
&& mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
&& echo 'extension=sqlsrv.so' >> "$PHP_INI_DIR/php.ini" \
&& echo 'extension=pdo_sqlsrv.so' >> "$PHP_INI_DIR/php.ini"

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer (php package manager)
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents to the working directory
COPY ./core/app /var/www/
RUN cp /var/www/.env.example /var/www/.env

# Instalamos dependendencias de composer
RUN composer install --no-ansi --no-dev --no-interaction --no-progress --optimize-autoloader --no-scripts

# Copy existing permissions from folder to docker
COPY --chown=www:www . /var/www
RUN chown -R www-data:www-data /var/www

# change current user to www
#@USER www
RUN chmod -R 777 /var/www/storage/logs


# Exponemos el puerto 9000 a la network
EXPOSE 9000

# Corremos el comando php-fpm para ejecutar PHP
CMD ["php-fpm"]
    
